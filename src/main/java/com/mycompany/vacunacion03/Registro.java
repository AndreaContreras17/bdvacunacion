/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vacunacion03;


import com.mycompany.vacunacion03.dao.VacunacionJpaController;
import com.mycompany.vacunacion03.dao.exceptions.NonexistentEntityException;
import com.mycompany.vacunacion03.entity.Vacunacion;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author andre
 */
@Path("inventario")
public class Registro {
    
     @GET
     @Produces(MediaType.APPLICATION_JSON)
    public Response listaIngresados(){
        
     /* Vacunacion RV=new Vacunacion();
      RV.setNombre("manuel");
      RV.setRut("5.749.812-8");
      RV.setTelefono("9763186");
      RV.setAlergia("no");
      
      Vacunacion VR2=new Vacunacion();
      VR2.setNombre("teresa");
      VR2.setRut("12.046.297-0");
      VR2.setTelefono("93247999");
      VR2.setAlergia("si");
      
      
      List<Vacunacion> listado=new ArrayList<Vacunacion>();
      
      listado.add(RV);
      listado.add(VR2);
*/
      VacunacionJpaController dao=new VacunacionJpaController();
        List<Vacunacion> listado = dao.findVacunacionEntities();
      
      return Response.ok(2).entity(listado).build();
      
      
      
    }
   
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response add(Vacunacion vacunacion){
        
         try {
             VacunacionJpaController dao=new VacunacionJpaController();
             dao.create(vacunacion);
         } catch (Exception ex) {
             Logger.getLogger(Registro.class.getName()).log(Level.SEVERE, null, ex);
         }
         
               return Response.ok(2).entity(vacunacion).build();

    }
    
    @DELETE
    @Path("/{iddelete}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("iddelete") String iddelete){
        
         try {
             VacunacionJpaController dao=new VacunacionJpaController();
             dao.destroy(iddelete);
         } catch (NonexistentEntityException ex) {
             Logger.getLogger(Registro.class.getName()).log(Level.SEVERE, null, ex);
         }
         
         return Response.ok("registro elimidado").build();
         
    }
    
    @PUT
    public Response update(Vacunacion vacunacion){
        
         try {
             VacunacionJpaController dao=new VacunacionJpaController();
             dao.edit(vacunacion);
         } catch (Exception ex) {
             Logger.getLogger(Registro.class.getName()).log(Level.SEVERE, null, ex);
         }
    
             return Response.ok(2).entity(vacunacion).build();

    }
    
}
