/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vacunacion03.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author andre
 */
@Entity
@Table(name = "vacunacion")
@NamedQueries({
    @NamedQuery(name = "Vacunacion.findAll", query = "SELECT v FROM Vacunacion v"),
    @NamedQuery(name = "Vacunacion.findByNombre", query = "SELECT v FROM Vacunacion v WHERE v.nombre = :nombre"),
    @NamedQuery(name = "Vacunacion.findByRut", query = "SELECT v FROM Vacunacion v WHERE v.rut = :rut"),
    @NamedQuery(name = "Vacunacion.findByDireccion", query = "SELECT v FROM Vacunacion v WHERE v.direccion = :direccion"),
    @NamedQuery(name = "Vacunacion.findByTelefono", query = "SELECT v FROM Vacunacion v WHERE v.telefono = :telefono"),
    @NamedQuery(name = "Vacunacion.findByAlergia", query = "SELECT v FROM Vacunacion v WHERE v.alergia = :alergia")})
public class Vacunacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Size(max = 2147483647)
    @Column(name = "nombre")
    private String nombre;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "rut")
    private String rut;
    @Size(max = 2147483647)
    @Column(name = "direccion")
    private String direccion;
    @Size(max = 2147483647)
    @Column(name = "telefono")
    private String telefono;
    @Size(max = 2147483647)
    @Column(name = "alergia")
    private String alergia;

    public Vacunacion() {
    }

    public Vacunacion(String rut) {
        this.rut = rut;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getAlergia() {
        return alergia;
    }

    public void setAlergia(String alergia) {
        this.alergia = alergia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rut != null ? rut.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vacunacion)) {
            return false;
        }
        Vacunacion other = (Vacunacion) object;
        if ((this.rut == null && other.rut != null) || (this.rut != null && !this.rut.equals(other.rut))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.vacunacion03.entity.Vacunacion[ rut=" + rut + " ]";
    }
    
}
